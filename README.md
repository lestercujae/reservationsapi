# README #


### What is this repository for? ###

* This is the repository of a Demo Exercise to manage Hotel reservations through a REST API
* 1.0


### How do I get set up? ###

* After succesfully cloned this repo run "mvn install" on the root folder or load the maven project using Spring Tools or Eclipse 
* Database: The database is a SQLite database. It will be created the first time the application is run


### Who do I talk to? ###

* Lester Hernandez (lestercujae@gmail.com)


### Standard used to write GIT commit messages ###
* Standard based on Conventional Commits 1.0.0 (https://www.conventionalcommits.org/en/v1.0.0/)


### Swagger ###
* This project contains a Swagger interface, it can be reviewed at: http://SERVER:PORT/swagger-ui.html (e. g. http://localhost:8080/swagger-ui.html)


### Project Overview ###

To facilitate the job of the reviewer/s it was decided to use an internal SQLite database.

The project is built in TIERS aiming to separate responsabilities through the whole system.

      +------------------------+ +---------------------+  +-------------+                     
      |                        | |                     |  |             |                     
      | BookingController      | | Swagger             |  |  DTOs       |   REST Layer        
      |                        | |                     |  |             |                     
      +------------------------+ +---------------------+  +-------------+                     
 --------------------------------------------------------------------------------------------
                                                                                              
       +-------------------------+                                                            
       |                         |                                                            
       | BookingService          |                                          Service Layer     
       |                         |                                                            
       +-------------------------+                                                            
 ---------------------------------------------------------------------------------------------
                                                                                              
       +--------------------+ +-----------------------+                                       
       |  Entity            | |                       |                                       
       |                    | |                       |                                       
       |  Booking           | | Booking Repository    |                     Data Acces Layer  
       |                    | |                       |                                       
       |                    | |                       |                                       
       +--------------------+ +-----------------------+                                       
   -------------------------------------------------------------------------------------------
              +---------------------+                                                         
              |                     |                                                         
              |                     |                                       Storage Layer     
              |  SQLite database    |                                                         
              |                     |                                                         
              |                     |                                                         
              +---------------------+                                                         

                             * the diagram was built using Textik (https://textik.com/)


### Shortcuts ###
Every shortcut taken during the project to save time is indentified with the prefix "SHORCUT APPLIED" followed by the explanation on why it was applied


### Pending Functionalities ###
* It was pending the integration with log4j to log to a txt file the details of each operation with the system.
* Some classes and methods does not contain the comments explaining their use


### Time Tracking ###
* During the execution of this project the developer used Toggl an online tool to time tracking. The log will be sent by email.

