package ca.alten.demo.reservations;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReservationsapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReservationsapiApplication.class, args);
	}

}
