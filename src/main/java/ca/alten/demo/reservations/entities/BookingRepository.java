/**
 * 
 */
package ca.alten.demo.reservations.entities;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * @author Lester Hernandez
 *
 */
@Repository
public interface BookingRepository extends JpaRepository<Booking, Integer> {
	
	@Query("SELECT count(*) FROM Booking b WHERE b.active = 1 AND ((:startDate >= b.startDate AND :startDate < b.endDate) OR (b.endDate < :endDate AND b.startDate > :startDate) OR (:endDate > b.startDate AND :endDate <= b.endDate))")
    public long checkIfPeriodIsAvailable(@Param("startDate") Date startDate, @Param("endDate") Date endDate);
	
	@Query("SELECT count(*) FROM Booking b WHERE b.active = 1 AND b.id <> :bookingId AND ((:startDate >= b.startDate AND :startDate < b.endDate) OR (b.endDate < :endDate AND b.startDate > :startDate) OR (:endDate > b.startDate AND :endDate <= b.endDate))")
    public long checkIfPeriodIsAvailableExcludingSelfBooking(@Param("startDate") Date startDate, @Param("endDate") Date endDate, @Param("bookingId") Integer bookingId);
}
