package ca.alten.demo.reservations.entities;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import ca.alten.demo.reservations.dto.BookingDTO;
import ca.alten.demo.reservations.dto.LightBookingDTO;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author Lester Hernandez
 *
 */
@Entity
public class Booking {
    @ApiModelProperty(notes = "Internal identifier of the Booking", example = "1", position = 1)
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
    
    @ApiModelProperty(notes = "Customer identifier", example = "CUST-000001", position = 2)
	private String customerIdentifier;
    
    @ApiModelProperty(notes = "Start date of the reservation", example = "2021-05-01", position = 3)
	private Date startDate;
    
    @ApiModelProperty(notes = "End date of the reservation", example = "2021-05-03", position = 4)
	private Date endDate;
    
    @ApiModelProperty(notes = "State of the reservation. 1=Active, 2=Cancelled", position = 5)
	private Integer active; //1 = ACTIVE, 0 = CANCELLED				
	
	public Booking() {
		
	}	
	
	public Booking(String customerIdentifier, Date startDate, Date endDate) {		
		this.customerIdentifier = customerIdentifier;
		this.startDate = startDate;
		this.endDate = endDate;
		this.active = 1;
	}
	
	public Booking(Integer id, String customerIdentifier, Date startDate, Date endDate) {
		this.id = id;
		this.customerIdentifier = customerIdentifier;
		this.startDate = startDate;
		this.endDate = endDate;
		this.active = 1;
	}
	
	public Booking(BookingDTO dto) throws ParseException {	
		this.id = dto.getBookingId();
		this.customerIdentifier = dto.getCustomerId();
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		
		this.startDate = format.parse(dto.getStartDate());
		this.endDate = format.parse(dto.getEndDate());
		
		this.active = 1;
	}
	
	public Booking(LightBookingDTO dto) throws ParseException {			
		this.customerIdentifier = dto.getCustomerId();
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		
		this.startDate = format.parse(dto.getStartDate());
		this.endDate = format.parse(dto.getEndDate());
		
		this.active = 1;
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getCustomerIdentifier() {
		return customerIdentifier;
	}
	
	public void setCustomerIdentifier(String customerIdentifier) {
		this.customerIdentifier = customerIdentifier;
	}
	
	public Date getStartDate() {
		return startDate;
	}
	
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	
	public Date getEndDate() {
		return endDate;
	}
	
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	public Integer getActive() {
		return active;
	}
	
	public void setActive(Integer active) {
		this.active = active;
	}
}

