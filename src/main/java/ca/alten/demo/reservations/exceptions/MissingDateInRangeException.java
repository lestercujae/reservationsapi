/**
 * 
 */
package ca.alten.demo.reservations.exceptions;

/**
 * @author Lester Hernandez
 *
 */
public class MissingDateInRangeException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1757000525303970873L;

}
