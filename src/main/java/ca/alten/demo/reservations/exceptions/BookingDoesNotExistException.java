package ca.alten.demo.reservations.exceptions;

/**
 * @author Lester Hernandez
 *
 */
public class BookingDoesNotExistException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8038986153593921998L;

}
