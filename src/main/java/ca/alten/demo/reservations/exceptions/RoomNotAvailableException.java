/**
 * 
 */
package ca.alten.demo.reservations.exceptions;

/**
 * @author Lester Hernandez
 *
 */
public class RoomNotAvailableException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7236130231417323802L;

}
