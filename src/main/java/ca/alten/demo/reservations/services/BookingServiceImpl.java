package ca.alten.demo.reservations.services;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ca.alten.demo.reservations.entities.Booking;
import ca.alten.demo.reservations.entities.BookingRepository;
import ca.alten.demo.reservations.exceptions.BookingDoesNotExistException;
import ca.alten.demo.reservations.exceptions.MaxReservationsDaysCountExceededException;
import ca.alten.demo.reservations.exceptions.MissingDateInRangeException;
import ca.alten.demo.reservations.exceptions.ReservartionWithMoreThanThirtyDaysInAdvanceException;
import ca.alten.demo.reservations.exceptions.ReservationStartingBeforeNextDay;
import ca.alten.demo.reservations.exceptions.RoomNotAvailableException;
import ca.alten.demo.reservations.exceptions.WrongDatesRangeException;
import ca.alten.demo.reservations.utils.DateUtils;

/**
 * @author Lester Hernandez
 * Service to manage the business logic of the hotel reservations
 */

@Service
public class BookingServiceImpl implements BookingService {
	@Autowired
	BookingRepository bookingRepository;
	
	@Override
	public boolean isRoomAvailable(Date startDate, Date endDate) throws WrongDatesRangeException, MissingDateInRangeException {
		//validations
		if(startDate == null || endDate == null) {
			throw new MissingDateInRangeException();
		}
		
		if(!DateUtils.areDatesInOrder(startDate, endDate)) {
			throw new WrongDatesRangeException();
		}
		
		//business logic
		return bookingRepository.checkIfPeriodIsAvailable(startDate, endDate) == 0;
	}

	@Override
	public Integer createBooking(Booking newBooking) throws MissingDateInRangeException, WrongDatesRangeException, MaxReservationsDaysCountExceededException, 
	                                                        ReservartionWithMoreThanThirtyDaysInAdvanceException, RoomNotAvailableException, ReservationStartingBeforeNextDay {
		//validations
		validateBooking(newBooking);
		
		if(!isRoomAvailable(newBooking.getStartDate(), newBooking.getEndDate())) {               //ROOM IS AVAILABLE
			throw new RoomNotAvailableException();
		}
		
		//business logic
		Booking savedBooking = bookingRepository.save(newBooking);
		
		return savedBooking.getId();
	}

	@Override
	public void updateBooking(Booking booking) throws BookingDoesNotExistException, MissingDateInRangeException, WrongDatesRangeException, 
	                                                  MaxReservationsDaysCountExceededException, ReservartionWithMoreThanThirtyDaysInAdvanceException, RoomNotAvailableException,
	                                                  ReservationStartingBeforeNextDay {
		//validations		
		if( !bookingRepository.existsById(booking.getId()) ){
			throw new BookingDoesNotExistException();
		}
		
		validateBooking(booking);

		if(!isRoomAvailable(booking.getStartDate(), booking.getEndDate(), booking.getId())) {     //ROOM IS AVAILABLE
			throw new RoomNotAvailableException();
		}
		
		//business logic
		bookingRepository.save(booking);
	}

	@Override
	public void cancelBooking(Integer bookingId) throws BookingDoesNotExistException {
		Optional<Booking> bookingOpt = bookingRepository.findById(bookingId);
		
		if(!bookingOpt.isPresent()) {
			throw new BookingDoesNotExistException();
		}
		
		bookingOpt.get().setActive(0); //mark the booking as CANCELLED
		
		bookingRepository.save(bookingOpt.get());
	}

	@Override
	public List<Booking> getAllBookings() {		
		return bookingRepository.findAll();
	}
	
	private void validateBooking(Booking booking) throws MissingDateInRangeException, WrongDatesRangeException, MaxReservationsDaysCountExceededException, 
	                                                     ReservartionWithMoreThanThirtyDaysInAdvanceException, RoomNotAvailableException, ReservationStartingBeforeNextDay{
		if(booking.getStartDate() == null || booking.getEndDate() == null) {               //NOT NULL DATES
			throw new MissingDateInRangeException();
		}
		
		if(!DateUtils.areDatesInOrder(booking.getStartDate(), booking.getEndDate())) {     //DATES IN ORDER
			throw new WrongDatesRangeException();
		}				
		
		if(DateUtils.getDaysDifference(booking.getStartDate(), booking.getEndDate()) > 3) {//MAX 3 DAYS
			throw new MaxReservationsDaysCountExceededException();
		}
		
		if(DateUtils.getDaysDifference(new Date(), booking.getStartDate()) > 30) {		   //NO MORE THAN 30 DAYS IN ADVANCE
			throw new ReservartionWithMoreThanThirtyDaysInAdvanceException();
		}
		
		if(DateUtils.getDaysDifference(new Date(), booking.getStartDate()) <= 0) {		   //NOT TODAY OR BEFORE
			throw new ReservationStartingBeforeNextDay();
		}																				    	
	}

	@Override
	public boolean isRoomAvailable(Date startDate, Date endDate, Integer bookingId)	throws WrongDatesRangeException, MissingDateInRangeException {
		///validations
		if(startDate == null || endDate == null) {
			throw new MissingDateInRangeException();
		}
		
		if(!DateUtils.areDatesInOrder(startDate, endDate)) {
			throw new WrongDatesRangeException();
		}
		
		//business logic
		return bookingRepository.checkIfPeriodIsAvailableExcludingSelfBooking(startDate, endDate, bookingId) == 0;
	}
}
