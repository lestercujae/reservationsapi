package ca.alten.demo.reservations.services;

import java.util.Date;
import java.util.List;

import ca.alten.demo.reservations.entities.Booking;
import ca.alten.demo.reservations.exceptions.BookingDoesNotExistException;
import ca.alten.demo.reservations.exceptions.MaxReservationsDaysCountExceededException;
import ca.alten.demo.reservations.exceptions.MissingDateInRangeException;
import ca.alten.demo.reservations.exceptions.ReservartionWithMoreThanThirtyDaysInAdvanceException;
import ca.alten.demo.reservations.exceptions.ReservationStartingBeforeNextDay;
import ca.alten.demo.reservations.exceptions.RoomNotAvailableException;
import ca.alten.demo.reservations.exceptions.WrongDatesRangeException;

/**
 * @author Lester Hernandez
 *
 */
public interface BookingService {
	public List<Booking> getAllBookings();
	
	public boolean isRoomAvailable(Date startDate, Date endDate) throws WrongDatesRangeException, MissingDateInRangeException;
	
	public boolean isRoomAvailable(Date startDate, Date endDate, Integer bookingId) throws WrongDatesRangeException, MissingDateInRangeException;
	
	public Integer createBooking(Booking newBooking) throws MissingDateInRangeException, WrongDatesRangeException, MaxReservationsDaysCountExceededException, 
	                                                        ReservartionWithMoreThanThirtyDaysInAdvanceException, RoomNotAvailableException, ReservationStartingBeforeNextDay;
	
	public void updateBooking(Booking booking) throws BookingDoesNotExistException, MissingDateInRangeException, WrongDatesRangeException, 
	                                                  MaxReservationsDaysCountExceededException, ReservartionWithMoreThanThirtyDaysInAdvanceException, RoomNotAvailableException,
	                                                  ReservationStartingBeforeNextDay;
	
	public void cancelBooking(Integer bookingId) throws BookingDoesNotExistException;
}
