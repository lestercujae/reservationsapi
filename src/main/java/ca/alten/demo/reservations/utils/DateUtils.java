/**
 * 
 */
package ca.alten.demo.reservations.utils;

import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * @author Lester Hernandez
 * Utility class to work with dates
 */
public class DateUtils {		
	public static long getDaysDifference(Date startDate, Date endDate) {
		long diff = endDate.getTime() - startDate.getTime();
		
	    return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
	}
	
	public static boolean areDatesInOrder(Date startDate, Date endDate) {
		return startDate.before(endDate);
	}
}
