package ca.alten.demo.reservations.rest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import ca.alten.demo.reservations.dto.BookingDTO;
import ca.alten.demo.reservations.dto.DateRangeDTO;
import ca.alten.demo.reservations.dto.LightBookingDTO;
import ca.alten.demo.reservations.entities.Booking;
import ca.alten.demo.reservations.exceptions.BookingDoesNotExistException;
import ca.alten.demo.reservations.exceptions.MaxReservationsDaysCountExceededException;
import ca.alten.demo.reservations.exceptions.MissingDateInRangeException;
import ca.alten.demo.reservations.exceptions.ReservartionWithMoreThanThirtyDaysInAdvanceException;
import ca.alten.demo.reservations.exceptions.ReservationStartingBeforeNextDay;
import ca.alten.demo.reservations.exceptions.RoomNotAvailableException;
import ca.alten.demo.reservations.exceptions.WrongDatesRangeException;
import ca.alten.demo.reservations.services.BookingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * @author Lester Hernandez
 *
 * The REST Controller of the application 
 * 
 * Methods can be reviewed and tested at 'http://localhost:8080/swagger-ui.html'
 */
@RestController 
public class BookingController {
	//SHORCUT APPLIED: If this was a real project I had used NUMERIC codes for each success or failure case while calling the verbs of the API, but to improve the  
	//                 readability of the results after calling any method of this API I decided to use TEXT messages  
	
	//constants for messages
	public static final String WRONG_DATES_FORMAT_MESSAGE = "Start Date and End Date must comply with the format 'yyyy-MM-dd'";
	public static final String BOOKING_TO_UPDATE_DOES_NOT_EXIST_MESSAGE = "The supplied Booking to update does not exist";
	public static final String BOOKING_TO_CANCEL_DOES_NOT_EXIST_MESSAGE = "The supplied Booking to cancel does not exist";
	public static final String WRONG_ORDER_IN_DATES_MESSAGE = "The 'End Date' must be greater than the 'Start Date'";
	public static final String NULL_DATES_MESSAGE = "The 'Start End' and the 'End Date' cannot be NULL";
	public static final String DAYS_IN_ADVANCE_MESSAGE = "A reservation cannot be booked with more than 30 Days in Advance";
	public static final String MAX_DAYS_MESSAGE = "A reservation cannot be longet than 3 days";
	public static final String ROOM_NOT_AVAILABLE_MESSAGE = "The room is not available in the dates range provided";
	public static final String NOT_BEFORE_TODAY_MESSAGE = "The starting date should be at least from the next day";
	public static final String ROOM_AVAILABLE_MESSAGE = "Room Available";
	public static final String ROOM_NO_AVAILABLE_MESSAGE = "Room No Available";
	public static final String SUCCESSFUL_UPDATE_MESSAGE = "Succesful Update Operation";
	public static final String SUCCESSFUL_CANCELATION_MESSAGE = "Succesful Cancelation Operation";
	
	
	//REST stuff
	@Autowired
	BookingService bookingService;
	
	@ApiOperation(value = "Retrieve a list of registered bookings", notes = "Returns a list of Booking")
	@GetMapping(value = "/bookings")
	public ResponseEntity<List<Booking>> findAll(){
		return ResponseEntity.ok(bookingService.getAllBookings()); 
	}
	
	@ApiOperation(value = "Check the availability of the room for a given period of time", response = String.class, notes = "Returns a message indicating the availability")
	@PostMapping(value = "/bookings/availability")
	public ResponseEntity<String> getAvailability(DateRangeDTO dto) {
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			
			Date startDate = format.parse(dto.getStartDate());
			Date endDate = format.parse(dto.getEndDate());
			
			String msg = ROOM_AVAILABLE_MESSAGE;
			
			if(!bookingService.isRoomAvailable(startDate, endDate)) {
				msg = ROOM_NO_AVAILABLE_MESSAGE;
			}			
			
			return ResponseEntity.ok(msg);
		} catch (ParseException e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, WRONG_DATES_FORMAT_MESSAGE); 
		} catch (WrongDatesRangeException e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, WRONG_ORDER_IN_DATES_MESSAGE);
		} catch (MissingDateInRangeException e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, NULL_DATES_MESSAGE);
		}				
	}
	
	@ApiOperation(value = "Book a reservation", response = Integer.class, notes = "Returns the ID of the new Booking object")
	@PostMapping(value = "/bookings")
	public ResponseEntity<Integer> create(LightBookingDTO newDTO){		
		try {
			Integer newBookingId = bookingService.createBooking(new Booking(newDTO));
			
			return ResponseEntity.ok(newBookingId); 
		} catch (ParseException e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, WRONG_DATES_FORMAT_MESSAGE); 
		} catch (MissingDateInRangeException e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, NULL_DATES_MESSAGE);
		} catch (WrongDatesRangeException e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, WRONG_ORDER_IN_DATES_MESSAGE);
		} catch (MaxReservationsDaysCountExceededException e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, MAX_DAYS_MESSAGE);
		} catch (ReservartionWithMoreThanThirtyDaysInAdvanceException e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, DAYS_IN_ADVANCE_MESSAGE);
		} catch (RoomNotAvailableException e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ROOM_NOT_AVAILABLE_MESSAGE);
		} catch (ReservationStartingBeforeNextDay e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, NOT_BEFORE_TODAY_MESSAGE);
		}				
	}
	
	@ApiOperation(value = "Update a booking", response = String.class, notes = "Updates a previous registered Booking object")
	@PutMapping(value = "/bookings")
	public ResponseEntity<String> update(BookingDTO dto){
		try {
			bookingService.updateBooking(new Booking(dto));
			
			return ResponseEntity.ok(SUCCESSFUL_UPDATE_MESSAGE);
		} catch (BookingDoesNotExistException e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, BOOKING_TO_UPDATE_DOES_NOT_EXIST_MESSAGE);
		} catch (ParseException e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, WRONG_DATES_FORMAT_MESSAGE);
		} catch (MissingDateInRangeException e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, NULL_DATES_MESSAGE);
		} catch (WrongDatesRangeException e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, WRONG_ORDER_IN_DATES_MESSAGE);
		} catch (MaxReservationsDaysCountExceededException e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, MAX_DAYS_MESSAGE);
		} catch (ReservartionWithMoreThanThirtyDaysInAdvanceException e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, DAYS_IN_ADVANCE_MESSAGE);
		} catch (RoomNotAvailableException e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ROOM_NOT_AVAILABLE_MESSAGE);
		} catch (ReservationStartingBeforeNextDay e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, NOT_BEFORE_TODAY_MESSAGE);
		}				
	}
	
	@ApiOperation(value = "Cancel a booking", response = String.class, notes = "Cancel a previous registered Booking")
	@DeleteMapping(value = "/bookings/{bookingId}")
	public ResponseEntity<String> delete(@PathVariable("bookingId") Integer bookingId) {
		try {
			bookingService.cancelBooking(bookingId);
			
			return ResponseEntity.ok(SUCCESSFUL_CANCELATION_MESSAGE);
		} catch (BookingDoesNotExistException e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, BOOKING_TO_CANCEL_DOES_NOT_EXIST_MESSAGE);
		}				
	}

}
