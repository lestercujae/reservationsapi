package ca.alten.demo.reservations.dto;

public class LightBookingDTO {
	protected String customerId;
	protected String startDate; //It must be in format yyyy-MM-dd
	protected String endDate;   //It must be in format yyyy-MM-dd
				
	public LightBookingDTO(String customerId, String startDate, String endDate) {		
		this.customerId = customerId;
		this.startDate = startDate;
		this.endDate = endDate;
	}	

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
}
