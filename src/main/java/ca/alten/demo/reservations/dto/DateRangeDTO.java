package ca.alten.demo.reservations.dto;

/**
 * @author Lester Hernandez
 *
 * DTO to use in availability requests 
 */
public class DateRangeDTO {
	private String startDate; //It must be in format yyyy-MM-dd
	private String endDate;   //It must be in format yyyy-MM-dd
	
	public DateRangeDTO(String startDate, String endDate) {
		super();
		this.startDate = startDate;
		this.endDate = endDate;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}		
}
