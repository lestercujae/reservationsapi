package ca.alten.demo.reservations.dto;


/**
 * @author Lester Hernandez
 * DTO exchanged through the REST API
 */
public class BookingDTO extends LightBookingDTO{
	private Integer bookingId;
	
	public BookingDTO(Integer bookingId, String customerId, String startDate, String endDate) {
		super(customerId, startDate, endDate);
		
		this.bookingId = bookingId;
		this.customerId = customerId;
		this.startDate = startDate;
		this.endDate = endDate;
	}

	public Integer getBookingId() {
		return bookingId;
	}

	public void setBookingId(Integer bookingId) {
		this.bookingId = bookingId;
	}	
}
