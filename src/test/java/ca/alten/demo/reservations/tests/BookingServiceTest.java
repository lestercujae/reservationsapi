package ca.alten.demo.reservations.tests;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import ca.alten.demo.reservations.entities.Booking;
import ca.alten.demo.reservations.entities.BookingRepository;
import ca.alten.demo.reservations.exceptions.BookingDoesNotExistException;
import ca.alten.demo.reservations.exceptions.MaxReservationsDaysCountExceededException;
import ca.alten.demo.reservations.exceptions.MissingDateInRangeException;
import ca.alten.demo.reservations.exceptions.ReservartionWithMoreThanThirtyDaysInAdvanceException;
import ca.alten.demo.reservations.exceptions.ReservationStartingBeforeNextDay;
import ca.alten.demo.reservations.exceptions.RoomNotAvailableException;
import ca.alten.demo.reservations.exceptions.WrongDatesRangeException;
import ca.alten.demo.reservations.services.BookingService;
import ca.alten.demo.reservations.services.BookingServiceImpl;


@RunWith(MockitoJUnitRunner.class)
class BookingServiceTest {
	//settings
	private SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");	

	@Mock
	private BookingRepository bookingRepository;
	
	@InjectMocks
	private BookingService bookingService = new BookingServiceImpl();
	
	private AutoCloseable closeable;
	
	@BeforeEach
    void initService() {
		closeable = MockitoAnnotations.openMocks(this);
		when(bookingRepository.save(Mockito.any(Booking.class))).thenAnswer((i -> i.getArguments()[0]));
    }
	
	@AfterEach
    void closeService() throws Exception {
        closeable.close();
    }
				
	//tests
	//availability
	@Test
	void testAvailability() throws Exception {
		doReturn(0L).when(bookingRepository).checkIfPeriodIsAvailable(Mockito.any(Date.class), Mockito.any(Date.class));
				
		assertEquals(true, bookingService.isRoomAvailable(format.parse("2021-04-12"), format.parse("2021-04-14")));
		
		//dates are null		
		assertThrows(MissingDateInRangeException.class, () -> { bookingService.isRoomAvailable(format.parse("2021-04-12"), null); } );
		assertThrows(MissingDateInRangeException.class, () -> { bookingService.isRoomAvailable(null, format.parse("2021-04-14")); } );
		assertThrows(MissingDateInRangeException.class, () -> { bookingService.isRoomAvailable(null, null); } );
		
		//wrong order
		assertThrows(WrongDatesRangeException.class, () -> { bookingService.isRoomAvailable(format.parse("2021-04-12"), format.parse("2021-04-10")); } );		
	}
	
	@Test
	void testCreateBooking() throws Exception {
		//basic save
		assertNotNull(bookingService.createBooking(new Booking(1, "CUST-000001", format.parse("2021-04-15"), format.parse("2021-04-17"))));
		
		//exceptions	
		assertThrows(MissingDateInRangeException.class, () -> { bookingService.createBooking(new Booking(1, "CUST-000001", format.parse("2021-04-15"), null)); } );
		assertThrows(MissingDateInRangeException.class, () -> { bookingService.createBooking(new Booking(1, "CUST-000001", null, format.parse("2021-04-17"))); } );
		assertThrows(MissingDateInRangeException.class, () -> { bookingService.createBooking(new Booking(1, "CUST-000001", null, null)); } );				
		assertThrows(WrongDatesRangeException.class, () -> { bookingService.createBooking(new Booking(1, "CUST-000001", format.parse("2021-04-25"), format.parse("2021-04-17"))); } );		
		assertThrows(MaxReservationsDaysCountExceededException.class, () -> { bookingService.createBooking(new Booking(1, "CUST-000001", format.parse("2021-04-15"), format.parse("2021-04-20"))); } );
		
		//stub availability
		doReturn(1L).when(bookingRepository).checkIfPeriodIsAvailable(Mockito.any(Date.class), Mockito.any(Date.class));		
		assertThrows(RoomNotAvailableException.class, () -> { bookingService.createBooking(new Booking(1, "CUST-000001", format.parse("2021-04-15"), format.parse("2021-04-17"))); } );
		
		//dynamic date stuff
		Date currentDate = new Date();				
		
		Calendar calendar = Calendar.getInstance();
		
        calendar.setTime(currentDate);        
        calendar.add(Calendar.DAY_OF_MONTH, 2);
		
		assertThrows(ReservationStartingBeforeNextDay.class, () -> { bookingService.createBooking(new Booking(1, "CUST-000001", currentDate, calendar.getTime())); } );

		calendar.setTime(currentDate); 
		calendar.add(Calendar.DAY_OF_MONTH, 35);
		Date startDate = calendar.getTime();
		calendar.add(Calendar.DAY_OF_MONTH, 2);
		
		assertThrows(ReservartionWithMoreThanThirtyDaysInAdvanceException.class, () -> { bookingService.createBooking(new Booking(1, "CUST-000001", startDate, calendar.getTime())); } );
	}
	
	@Test
	void testUpdateBooking() throws Exception {
		doReturn(true).when(bookingRepository).existsById(Mockito.any(Integer.class));
		doReturn(0L).when(bookingRepository).checkIfPeriodIsAvailableExcludingSelfBooking(Mockito.any(Date.class), Mockito.any(Date.class), Mockito.any(Integer.class));
		
		//basic update
		assertDoesNotThrow(() -> { bookingService.updateBooking(new Booking(1, "CUST-000001", format.parse("2021-04-15"), format.parse("2021-04-17"))); });
		
		//exceptions	
		assertThrows(MissingDateInRangeException.class, () -> { bookingService.updateBooking(new Booking(1, "CUST-000001", format.parse("2021-04-15"), null)); } );
		assertThrows(MissingDateInRangeException.class, () -> { bookingService.updateBooking(new Booking(1, "CUST-000001", null, format.parse("2021-04-17"))); } );
		assertThrows(MissingDateInRangeException.class, () -> { bookingService.updateBooking(new Booking(1, "CUST-000001", null, null)); } );				
		assertThrows(WrongDatesRangeException.class, () -> { bookingService.updateBooking(new Booking(1, "CUST-000001", format.parse("2021-04-25"), format.parse("2021-04-17"))); } );		
		assertThrows(MaxReservationsDaysCountExceededException.class, () -> { bookingService.updateBooking(new Booking(1, "CUST-000001", format.parse("2021-04-15"), format.parse("2021-04-20"))); } );
		
		//stub availability
		doReturn(1L).when(bookingRepository).checkIfPeriodIsAvailableExcludingSelfBooking(Mockito.any(Date.class), Mockito.any(Date.class), Mockito.any(Integer.class));		
		assertThrows(RoomNotAvailableException.class, () -> { bookingService.updateBooking(new Booking(1, "CUST-000001", format.parse("2021-04-15"), format.parse("2021-04-17"))); } );				
		
		//dynamic date stuff
		Date currentDate = new Date();				
		
		Calendar calendar = Calendar.getInstance();
		
        calendar.setTime(currentDate);        
        calendar.add(Calendar.DAY_OF_MONTH, 2);
		
		assertThrows(ReservationStartingBeforeNextDay.class, () -> { bookingService.updateBooking(new Booking(1, "CUST-000001", currentDate, calendar.getTime())); } );

		calendar.setTime(currentDate); 
		calendar.add(Calendar.DAY_OF_MONTH, 35);
		Date startDate = calendar.getTime();
		calendar.add(Calendar.DAY_OF_MONTH, 2);
		
		assertThrows(ReservartionWithMoreThanThirtyDaysInAdvanceException.class, () -> { bookingService.updateBooking(new Booking(1, "CUST-000001", startDate, calendar.getTime())); } );
		
		//does not exist
		doReturn(false).when(bookingRepository).existsById(Mockito.any(Integer.class));
		assertThrows(BookingDoesNotExistException.class, () -> { bookingService.updateBooking(new Booking(1, "CUST-000001", format.parse("2021-04-15"), format.parse("2021-04-17"))); } );
	}
	
	@Test
	void testCancelBooking() throws Exception {		
		doReturn( Optional.of(new Booking(1, "CUST-000001", format.parse("2021-04-15"), format.parse("2021-04-17"))) ).when(bookingRepository).findById(Mockito.any(Integer.class));
		
		//basic
		assertDoesNotThrow(() -> { bookingService.cancelBooking(1); });
		
		//exceptions
		doReturn( Optional.ofNullable(null) ).when(bookingRepository).findById(Mockito.any(Integer.class));
		assertThrows(BookingDoesNotExistException.class, () -> { bookingService.cancelBooking(1); } );
	}
	
	//get all
	@Test
	void testFindAll() throws ParseException {
		List<Booking> bookingsRep = Arrays.asList(
				new Booking(1, "CUST-000001", format.parse("2021-04-15"), format.parse("2021-04-17")),
				new Booking(2, "CUST-000002", format.parse("2021-04-21"), format.parse("2021-04-22")),
				new Booking(3, "CUST-000003", format.parse("2021-04-24"), format.parse("2021-04-26")),
				new Booking(4, "CUST-000004", format.parse("2021-04-27"), format.parse("2021-04-28"))
				);				
		
		//mocks
		doReturn(bookingsRep).when(bookingRepository).findAll();
		
		List<Booking> bookings = bookingService.getAllBookings();
				
		assertEquals(bookingsRep, bookings);					
	}
	
	
	
}
