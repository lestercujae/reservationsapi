package ca.alten.demo.reservations.tests;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import ca.alten.demo.reservations.utils.DateUtils;

/**
 * To test quickly the methods of the DateUtils utility class
 * 
 * @author Lester Hernandez
 *
 */
public class DateUtilsTests {

	public static void main(String[] args) throws ParseException {
		//difference		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		
		Date dateStart = format.parse("2021-04-15");
		Date dateEnd = format.parse("2021-04-16");
		
		long diff = DateUtils.getDaysDifference(dateStart, dateEnd);
		
		System.out.println(diff);
		
		Date dateStart2 = format.parse("2021-04-09");
		diff = DateUtils.getDaysDifference(dateStart2, new Date());
		
		System.out.println(diff);
	}

}
