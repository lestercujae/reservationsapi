package ca.alten.demo.reservations.tests;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ca.alten.demo.reservations.entities.Booking;
import ca.alten.demo.reservations.exceptions.BookingDoesNotExistException;
import ca.alten.demo.reservations.exceptions.MaxReservationsDaysCountExceededException;
import ca.alten.demo.reservations.exceptions.MissingDateInRangeException;
import ca.alten.demo.reservations.exceptions.ReservartionWithMoreThanThirtyDaysInAdvanceException;
import ca.alten.demo.reservations.exceptions.ReservationStartingBeforeNextDay;
import ca.alten.demo.reservations.exceptions.RoomNotAvailableException;
import ca.alten.demo.reservations.exceptions.WrongDatesRangeException;
import ca.alten.demo.reservations.rest.BookingController;
import ca.alten.demo.reservations.services.BookingService;

@RunWith(SpringRunner.class)
@WebMvcTest(value = BookingController.class)
class RestControllerTest {
	private SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
	
	@Autowired
	private MockMvc mvc;
	
	@MockBean
	private BookingService bookingService;
	
	@Test
	final void testFindAll() throws Exception {
		List<Booking> bookingsRep = Arrays.asList(
				new Booking(1, "CUST-000001", format.parse("2021-04-15"), format.parse("2021-04-17")),
				new Booking(2, "CUST-000002", format.parse("2021-04-21"), format.parse("2021-04-22")),
				new Booking(3, "CUST-000003", format.parse("2021-04-24"), format.parse("2021-04-26")),
				new Booking(4, "CUST-000004", format.parse("2021-04-27"), format.parse("2021-04-28"))
				);

		when(bookingService.getAllBookings()).thenReturn(bookingsRep);

		//test
		String uri = "/bookings";
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
		
		String content = mvcResult.getResponse().getContentAsString();
	    
		Booking[] bookings = mapFromJson(content, Booking[].class);
		
	    assertEquals(4, bookings.length);
	}

	
	@Test
	final void testGetAvailability() throws Exception {			
		String uri = "/bookings/availability?endDate=2021-05-22&startDate=2021-05-20";
		
		//available
		when(bookingService.isRoomAvailable(Mockito.any(Date.class), Mockito.any(Date.class))).thenReturn(true);						
		
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content("")).andReturn();		
		assertEquals(BookingController.ROOM_AVAILABLE_MESSAGE, mvcResult.getResponse().getContentAsString());
		
		//not available
		when(bookingService.isRoomAvailable(Mockito.any(Date.class), Mockito.any(Date.class))).thenReturn(false);
		
		mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content("")).andReturn();				
		assertEquals(BookingController.ROOM_NO_AVAILABLE_MESSAGE, mvcResult.getResponse().getContentAsString());
		
		//special cases
		//missing dates
		when(bookingService.isRoomAvailable(Mockito.any(Date.class), Mockito.any(Date.class))).thenThrow(MissingDateInRangeException.class);
		
		mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content("")).andReturn();		
		assertEquals(BookingController.NULL_DATES_MESSAGE, mvcResult.getResponse().getErrorMessage());
						
		//wrong order
		when(bookingService.isRoomAvailable(Mockito.any(Date.class), Mockito.any(Date.class))).thenThrow(WrongDatesRangeException.class);
		
		mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content("")).andReturn();		
		assertEquals(BookingController.WRONG_ORDER_IN_DATES_MESSAGE, mvcResult.getResponse().getErrorMessage());
		
		//wrong format
		uri = "/bookings/availability?endDate=2021&startDate=2021-05-20";
		
		mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content("")).andReturn();		
		assertEquals(BookingController.WRONG_DATES_FORMAT_MESSAGE, mvcResult.getResponse().getErrorMessage());
	}

	
	@Test
	final void testCreate() throws Exception {
		String uri = "/bookings?customerId=CUST0001&endDate=2021-04-17&startDate=2021-04-15";
		
		//create
		when(bookingService.createBooking(Mockito.any(Booking.class))).thenReturn(2);						
		
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content("")).andReturn();		
		assertEquals("2", mvcResult.getResponse().getContentAsString());	
		
		//special cases
		//not before
		when(bookingService.createBooking(Mockito.any(Booking.class))).thenThrow(ReservationStartingBeforeNextDay.class);
		
		mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content("")).andReturn();		
		assertEquals(BookingController.NOT_BEFORE_TODAY_MESSAGE, mvcResult.getResponse().getErrorMessage());		
		
		//no available
		when(bookingService.createBooking(Mockito.any(Booking.class))).thenThrow(RoomNotAvailableException.class);
		
		mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content("")).andReturn();		
		assertEquals(BookingController.ROOM_NOT_AVAILABLE_MESSAGE, mvcResult.getResponse().getErrorMessage());
		
		//days in advance
		when(bookingService.createBooking(Mockito.any(Booking.class))).thenThrow(ReservartionWithMoreThanThirtyDaysInAdvanceException.class);
		
		mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content("")).andReturn();		
		assertEquals(BookingController.DAYS_IN_ADVANCE_MESSAGE, mvcResult.getResponse().getErrorMessage());
		
		//max days
		when(bookingService.createBooking(Mockito.any(Booking.class))).thenThrow(MaxReservationsDaysCountExceededException.class);
		
		mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content("")).andReturn();		
		assertEquals(BookingController.MAX_DAYS_MESSAGE, mvcResult.getResponse().getErrorMessage());
		
		//wrong order
		when(bookingService.createBooking(Mockito.any(Booking.class))).thenThrow(WrongDatesRangeException.class);
		
		mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content("")).andReturn();		
		assertEquals(BookingController.WRONG_ORDER_IN_DATES_MESSAGE, mvcResult.getResponse().getErrorMessage());
		
		//missing dates
		when(bookingService.createBooking(Mockito.any(Booking.class))).thenThrow(MissingDateInRangeException.class);
		
		mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content("")).andReturn();		
		assertEquals(BookingController.NULL_DATES_MESSAGE, mvcResult.getResponse().getErrorMessage());
		
		//wrong date formats
		uri = "/bookings?customerId=CUST0001&endDate=2021-04-17&startDate=2021";
		
		mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content("")).andReturn();		
		assertEquals(BookingController.WRONG_DATES_FORMAT_MESSAGE, mvcResult.getResponse().getErrorMessage());						
	}

	@Test
	final void testUpdate() throws Exception {
		String uri = "/bookings?bookingId=2&customerId=CUST0001&endDate=2021-04-17&startDate=2021-04-15";
		
		//update										
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content("")).andReturn();		
		assertEquals(BookingController.SUCCESSFUL_UPDATE_MESSAGE, mvcResult.getResponse().getContentAsString());	
		
		//special cases
		//not before
		doThrow(new BookingDoesNotExistException()).when(bookingService).updateBooking( Mockito.any(Booking.class) );
		
		mvcResult = mvc.perform(MockMvcRequestBuilders.put(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content("")).andReturn();		
		assertEquals(BookingController.BOOKING_TO_UPDATE_DOES_NOT_EXIST_MESSAGE, mvcResult.getResponse().getErrorMessage());
		
		//not before
		doThrow(new ReservationStartingBeforeNextDay()).when(bookingService).updateBooking( Mockito.any(Booking.class) );
		
		mvcResult = mvc.perform(MockMvcRequestBuilders.put(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content("")).andReturn();		
		assertEquals(BookingController.NOT_BEFORE_TODAY_MESSAGE, mvcResult.getResponse().getErrorMessage());		
		
		//no available
		doThrow(new RoomNotAvailableException()).when(bookingService).updateBooking( Mockito.any(Booking.class) );
		
		mvcResult = mvc.perform(MockMvcRequestBuilders.put(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content("")).andReturn();		
		assertEquals(BookingController.ROOM_NOT_AVAILABLE_MESSAGE, mvcResult.getResponse().getErrorMessage());
		
		//days in advance
		doThrow(new ReservartionWithMoreThanThirtyDaysInAdvanceException()).when(bookingService).updateBooking( Mockito.any(Booking.class) );
		
		mvcResult = mvc.perform(MockMvcRequestBuilders.put(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content("")).andReturn();		
		assertEquals(BookingController.DAYS_IN_ADVANCE_MESSAGE, mvcResult.getResponse().getErrorMessage());
		
		//max days
		doThrow(new MaxReservationsDaysCountExceededException()).when(bookingService).updateBooking( Mockito.any(Booking.class) );
		
		mvcResult = mvc.perform(MockMvcRequestBuilders.put(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content("")).andReturn();		
		assertEquals(BookingController.MAX_DAYS_MESSAGE, mvcResult.getResponse().getErrorMessage());
		
		//wrong order
		doThrow(new WrongDatesRangeException()).when(bookingService).updateBooking( Mockito.any(Booking.class) );
		
		mvcResult = mvc.perform(MockMvcRequestBuilders.put(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content("")).andReturn();		
		assertEquals(BookingController.WRONG_ORDER_IN_DATES_MESSAGE, mvcResult.getResponse().getErrorMessage());
		
		//missing dates
		doThrow(new MissingDateInRangeException()).when(bookingService).updateBooking( Mockito.any(Booking.class) );
		
		mvcResult = mvc.perform(MockMvcRequestBuilders.put(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content("")).andReturn();		
		assertEquals(BookingController.NULL_DATES_MESSAGE, mvcResult.getResponse().getErrorMessage());
		
		//wrong date formats
		uri = "/bookings?bookingId=2&customerId=CUST0001&endDate=2021&startDate=2021-04-15";
		
		mvcResult = mvc.perform(MockMvcRequestBuilders.put(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content("")).andReturn();		
		assertEquals(BookingController.WRONG_DATES_FORMAT_MESSAGE, mvcResult.getResponse().getErrorMessage());
	}

	@Test
	final void testDelete() throws Exception {
		String uri = "/bookings/4";
		
		//cancel
		doNothing().when(bookingService).cancelBooking( Mockito.any(Integer.class) );
		
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content("")).andReturn();		
		assertEquals(BookingController.SUCCESSFUL_CANCELATION_MESSAGE, mvcResult.getResponse().getContentAsString());				
		
		//special cases
		//missing dates
		doThrow(new BookingDoesNotExistException()).when(bookingService).cancelBooking( Mockito.any(Integer.class) );
		
		mvcResult = mvc.perform(MockMvcRequestBuilders.delete(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content("")).andReturn();		
		assertEquals(BookingController.BOOKING_TO_CANCEL_DOES_NOT_EXIST_MESSAGE, mvcResult.getResponse().getErrorMessage());
	}	
	
	private <T> T mapFromJson(String json, Class<T> clazz) throws JsonParseException, JsonMappingException, IOException {

		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.readValue(json, clazz);
	}
	
}
